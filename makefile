LATEX = pdflatex
BIB = biber
OUTPUT_DIR = doc
MAIN_DOCUMENT = model

doc:
	mkdir -p ${OUTPUT_DIR}
	${LATEX} -output-directory ${OUTPUT_DIR} ${MAIN_DOCUMENT}.tex
	${BIB} --output_directory ${OUTPUT_DIR} ${MAIN_DOCUMENT}
	${LATEX} -output-directory ${OUTPUT_DIR} ${MAIN_DOCUMENT}.tex

clean:
	rm -rf ${OUTPUT_DIR}

.PHONY: doc clean