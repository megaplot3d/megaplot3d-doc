In the project directory, you can run:
```bash
make doc
```
Creates `doc` directory to store all the temporary files, compiles project to `doc\model.pdf`
```bash
make clean
```
Removes `doc` folder and its contents
